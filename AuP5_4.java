import java.util.Arrays;

public class Main {


    public static void markovCycle(String text, String[][] Regeln){
        while (true) {
            System.out.print(text+" -> ");
            for (int i = 0; i <= Regeln.length; i++){
                if (text.contains(Regeln[i][0])){
                    text = text.replaceFirst("\\Q"+Regeln[i][0]+"\\E", Regeln[i][1]);
                    System.out.print(text+"     Regel: "+i+"\n");
                    if (Regeln[i].length == 3){
                            System.out.println("\nDone");
                            return;
                    }
                    break;
                }
            }
        }
    }

    public static void main(String[] args) {
        if (args.length == 0){
            System.out.println("Keine Argumente eingegeben, bitte mit Argumenten Starten\nBeispiel: \"|||\" \"'#|','||#'\" \"'#','','.'\" \"'|','#|'\"");
            return;
        }
        for (int i= 0; i< args.length; i++){
            args[i] = args[i].replace("'","");
        }
        String[][] regeln = new String[(args.length-1)][];
        for (int i = 1; i < args.length; i++){
            regeln[i-1] = args[i].split(",");
        }
        String input = args[0];
        markovCycle(input, regeln);
    }

}
