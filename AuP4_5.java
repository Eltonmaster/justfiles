import java.util.Arrays;

public class Main {

    public static int [][] recpartition(int i, int[] a, int[] b, int[] c){

        int sum_b = 0;
        int sum_c = 0;

        for (int j:b){
            sum_b+=j;
        }
        for (int j:c){
            sum_c+=j;
        }

        if (i >= a.length){
            if ((sum_b == sum_c) && (b.length > 0) && (c.length > 0)  ){
                return array_maker(b,c);
            }else{
                return null;
            }
        }

        int temp_value = a[i];
        int sum_b_val = sum_b+temp_value;
        int sum_c_val = sum_c+temp_value;

        if (sum_b_val > sum_c_val){
            int [][] temp1 = recpartition(i+1, a, b, longify(c, temp_value));
            if (temp1 == null){
                int[][] temp2 = recpartition(i+1, a, longify(b, temp_value), c);
                if (temp2 == null){
                    return null;
                } else {
                    return temp2;
                }
            } else {
                return temp1;
            }
        }else{
            int[][] temp1 = recpartition(i+1, a, longify(b, temp_value), c);
            if (temp1 == null){
                int[][] temp2 = recpartition(i+1, a, b, longify(c, temp_value));
                if (temp2 == null){
                    return null;
                } else {
                    return temp2;
                }
            } else {
                return temp1;
            }
        }

    }

    public  static int[][] array_maker(int[] x, int[] y){
        int[][] temp = new int[2][];
        temp[0] = new int[x.length];
        temp[1] = new int[y.length];
        if (x.length > 0){
            for(int i = 0; i < x.length; i++){
                temp[0][i]=x[i];
            }
        }
        if (y.length > 0){
            for(int i = 0; i < y.length; i++){
                temp[1][i]=y[i];
            }
        }
        return temp;
    }

    public static int[] longify(int[] z, int x){
        int n = z.length;
        int new_arr[] = new int[n+1];
        for (int i = 0; i < z.length; i++){
            new_arr[i] = z[i];
        }
        new_arr[n] = x;
        return new_arr;
    }

    public static int[][] partition(int[] a) {
        int[] b = {};
        int[] c = {};
        int[][] answer = recpartition(0, a ,b , c);
        if (answer == null){
            System.out.println("Es gibt keine Lösung");
            return null;
        } else {
            System.out.println("Lösung gefunden für:\na = "+Arrays.toString(a)+"\nb = "+Arrays.toString(answer[0])+"\nc = "+Arrays.toString(answer[1]));
            return answer;
        }
    }

}
