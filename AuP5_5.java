public class Main {

    public static void main(String[] args) {
        if (args.length == 0){
            System.out.println("Keine Zahl eingegeben");
            return;
        }
        int input = 0;
        if (args.length == 1){
            input = Integer.parseInt(args[0]);
        }
        double temp = 1;
        while (true){
           if (temp*temp < input - 0.0000001){
               double a = temp/2;
               double b = 3-((temp*temp)/input);
               temp = a*b;
            } else {
               System.out.println(temp);
               return;
           }
        }
    }

}
