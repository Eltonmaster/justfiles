static class List{
        private int[] sub_list;

        public List(){}

        public List(int[] a){
            this.sub_list = a;
        }

        public int head(){
            return sub_list[0];
        }

        public int rhead(){
            return sub_list[sub_list.length-1];
        }

        public int[] rtail(){
            int[] temp_list = new int[sub_list.length-1];
            for (int i = 0; i < temp_list.length; i++){
                temp_list[i] = sub_list[i];
            }
            return temp_list;
        }

        public int[] tail(){
            int[] temp_list = new int[sub_list.length-1];
            for (int i = 0; i < temp_list.length; i++){
                temp_list[i] = sub_list[i+1];
            }
            return temp_list;
        }

        public int length(){
            return sub_list.length;
        }

        public void print(){
            for (int i = 0; i < sub_list.length; i++){
                System.out.print(sub_list[i]+", ");
            }
            System.out.println("\n\n");
        }

        public void push_front(int a){
            int[] temp_list = new int[sub_list.length+1];
            temp_list[0] = a;
            for (int i = 1; i < temp_list.length; i++){
                temp_list[i] = sub_list[i-1];
            }
            sub_list = temp_list;
        }

        public boolean is_sorted(){
            int temp = Integer.MIN_VALUE;
            for (int i = 0; i < sub_list.length; i++){
                if (sub_list[i] >= temp){
                    temp = sub_list[i];
                } else {
                    return false;
                }
            }
            return true;
        }
    }