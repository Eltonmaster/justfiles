Private Sub Worksheet_Change(ByVal Target As Range)
    'Variable1: Die Zellenrange die überprüft werden soll          Variable2: Die Nummer der Ellipse
    Call SetColour(Range("A1:A5"), 1)
    Call SetColour(Range("B1:B5"), 2)
    Call SetColour(Range("C1:C5"), 3)

End Sub


Sub SetColour(entries As Range, ellipsis As Integer)
    farbe = "grün"
    For Each c In entries.Cells
        If c.Value = "nicht befähigt" Then
            farbe = "rot"
        End If
        If c.Value = "bedingt befähigt" And farbe <> "rot" Then
            farbe = "gelb"
        End If
    Next
    If farbe = "grün" Then
        ActiveSheet.Shapes("Ellipse " & CStr(ellipsis)).Fill.ForeColor.RGB = RGB(20, 166, 3)
    End If
    If farbe = "rot" Then
        ActiveSheet.Shapes("Ellipse " & CStr(ellipsis)).Fill.ForeColor.RGB = RGB(255, 0, 0)
    End If
    If farbe = "gelb" Then
        ActiveSheet.Shapes("Ellipse " & CStr(ellipsis)).Fill.ForeColor.RGB = RGB(214, 158, 4)
    End If

End Sub
