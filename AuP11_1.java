import java.util.Arrays;

public class Main{

    static class List{
        private int[] sub_list;

        public List(){
            sub_list = new int[0];
        }

        public List(int[] a){
            this.sub_list = a;
        }

        public int head(){
            return sub_list[0];
        }

        public int rhead(){
            return sub_list[sub_list.length-1];
        }

        public List rtail(){
            int[] temp_list = new int[sub_list.length-1];
            for (int i = 0; i < temp_list.length; i++){
                temp_list[i] = sub_list[i];
            }
            return new List(temp_list);
        }

        public List tail(){
            int[] temp_list = new int[sub_list.length-1];
            for (int i = 0; i < temp_list.length; i++){
                temp_list[i] = sub_list[i+1];
            }
            return new List(temp_list);
        }

        public void remove_last(){
            int[] temp_list = new int[sub_list.length-1];
            for (int i = 1; i <= temp_list.length; i++){
                temp_list[i-1] = sub_list[i];
            }
            sub_list = temp_list;
        }

        public boolean is_empty(){
            return sub_list.length == 0;
        }

        public int length(){
            return sub_list.length;
        }

        public String toString(){
            String temp = "";
            for (int i : sub_list){
                temp += Integer.toString(i)+"  ";
            }

            return temp;
        }

        public void print(){
            for (int i = 0; i < sub_list.length; i++){
                System.out.print(sub_list[i]+", ");
            }
            System.out.println("\n\n");
        }

        public void push_front(int a){
            int[] temp_list = new int[sub_list.length+1];
            temp_list[0] = a;
            for (int i = 1; i < temp_list.length; i++){
                temp_list[i] = sub_list[i-1];
            }
            sub_list = temp_list;
        }

        public boolean is_sorted(){
            int temp = Integer.MIN_VALUE;
            for (int i = 0; i < sub_list.length; i++){
                if (sub_list[i] >= temp){
                    temp = sub_list[i];
                } else {
                    return false;
                }
            }
            return true;
        }
    }

    static class Stack {
        List sub_stack;

        public Stack(){
            sub_stack = new List();
        }

        public Stack push(int a) {
            sub_stack.push_front(a);
            return this;
        }

        public Stack pop() {
            sub_stack.remove_last();
            return this;
        }

        public int top() {
            return sub_stack.rhead();
        }

        public Boolean is_empty(){
            return sub_stack.is_empty();
        }

        public void print(){
            System.out.println(sub_stack.toString());
        }

    }

    public static void main(String[] args) {

        Stack wow = new Stack();

        System.out.println(Boolean.toString(wow.is_empty()));
        wow.push(2);
        wow.push(4);
        wow.print();
        wow.push(6);
        wow.print();
        wow.pop();
        wow.print();
        wow.pop();
        wow.print();
        System.out.println(Boolean.toString(wow.is_empty()));

    }
}
