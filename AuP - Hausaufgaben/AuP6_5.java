public class Main {

    public static void main(String[] args) {
        if (args.length <= 1){
            System.out.println("Bitte geben Sie 2 Zahlen als Argumente ein");
            return;
        }
        if (args.length >= 3){
            System.out.println("Bitte geben Sie 2 Zahlen als Argumente ein");
            return;
        }
        double a, b, f_x, g_x ;
        int count = 0;
        a = Double.parseDouble(args[0]);
        b = Double.parseDouble(args[1]);

        if ((a <= 1) || (b <= 1)){
            System.out.println("die Beiden Zahlen müssen >1 sein");
            return;
        }
        System.out.println("a= " + a);
        System.out.println("b= " + b);

        while (true){
            f_x = Math.pow(count,a);
            g_x = Math.pow(b,count);

            System.out.println("\nf("+ count +") = " + f_x);
            System.out.println("g("+ count +") = " + g_x);

            if ((f_x < g_x) && (count >= 2)){
                System.out.println("\nf(x) < g(x) bei x = " + count);
                return;
            }
            count++;
        }

    }

}
