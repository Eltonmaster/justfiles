import os, sys, json
from xml.dom import minidom
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from time import sleep
from zipfile import ZipFile
import requests
import tkinter
from tkinter import messagebox, filedialog
from functools import partial

class UpdateButton:
    def __init__(self, where):
        self.widget = tkinter.Button(where, text="Update Mods", width=40, command=update_mods)
        self.widget.grid(column=0, row=1, pady=(0,5))

class Modentry:
    def __init__(self, where, text, row):
        self.mod_name = text
        self.text_string = tkinter.StringVar()
        self.text_string.set(text)
        self.icon_string = tkinter.StringVar()
        self.icon_string.set("")
        self.label = tkinter.Label(where.widget, textvariable=self.text_string)
        self.label.grid(column=0, row=row, sticky="W")
        self.icon = tkinter.Label(where.widget, textvariable=self.icon_string)
        self.icon.grid(column=1, row=row)
        self.menu = tkinter.Menu(self.label, tearoff=0)
        self.menu.add_command(label="Remove Mod", command=self.remove)
        self.label.bind("<Button-3>", self.show_menu)
        where.length += 1        

    def show_menu(self, event):
        self.menu.tk_popup(event.x_root+30, event.y_root+10, 0)

    def remove(self):
        global config, group
        for entry in config["mods"]:
            if entry["title"] == self.mod_name:
                config["mods"].remove(entry)
                with open(config["config_path"], "w") as f:
                    f.write(json.dumps(config))
        self.label.destroy()
        self.icon.destroy()
        group.widget.update()
        print("removed")

    def update_text(self, text):
        self.text_string.set(text)
        self.update_icon("done")
        print("set variable")

    def update_icon(self, state):
        if state == "loading":
            self.icon_string.set("⌛")
        elif state == "done":
            self.icon_string.set("✔")
        elif state == "warning":
            self.icon_string.set("⚠️")
        elif state == "cross":
            self.icon_string.set("❌")
        elif state == "empty":
            self.icon_string.set("")

class Group:
    def __init__(self, where, title):
        self.length = 0
        self.entry_open = False
        self.widget = tkinter.LabelFrame(where, text=title)
        self.widget.grid(column=0, row=0, padx=20,pady=10)
        self.widget.columnconfigure(0, minsize=250, weight=5)
        self.widget.columnconfigure(1, weight=1)
        # self.button = tkinter.Button(self.widget, text="Add Mod", command=self.add_entry)
        # self.button.grid(column=0, columnspan=2, row=self.length+3, pady=5)
        self.menu = tkinter.Menu(self.widget, tearoff=0)
        self.menu.add_command(label="Add Mod", command=self.add_entry)
        self.widget.bind("<Button-3>", self.show_menu)

    def show_menu(self, event):
        self.menu.tk_popup(event.x_root+30, event.y_root+10, 0)

    def add_entry(self):
        if self.entry_open:
            if self.entry.get().startswith("http"):
                print(self.entry.get())
                title = mod_to_config(self.entry.get().strip())
                self.entry.destroy()
                Modentry(self, title, self.length+1)
                self.entry_open = not self.entry_open
                return 
            else:
                print("no mod link")
                return 
        else:
            self.entry = tkinter.Entry(self.widget, width=30)
            self.entry.insert(0, "WGMODS.NET URL")
            self.entry.grid(column=0, columnspan=2, row=self.length+2, sticky="w")
            self.entry_open = not self.entry_open
    
    def populate(self, config):
        temp = []
        if len(config["mods"])==0:
            self.add_entry()
            return
        for entry in config["mods"]:
            temp.append(Modentry(self, entry["title"], self.length))
        return temp

def ShowInfo(title, text):
    messagebox.showinfo(title, text)

def Select_folder(title):
    return filedialog.askdirectory(title=title, mustexist=True).strip()

def mod_to_config(url):
    global driver, config
    driver.get(url)
    title = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, "Details_clampLines--2ZO-m"))).text.strip()
    mod = {"title":title, "url":url, "version":"0.0.0.0"}
    config["mods"].append(mod)
    print(f"Added: {title}")
    with open(config["config_path"], "w") as f:
        f.write(json.dumps(config))
    return title

def download(url):
    print(f"downloading {url}")
    with requests.get(url, stream=True) as req:
        req.raise_for_status()
        with open("temp.zip", "wb") as f:
            for chunk in req.iter_content(chunk_size=1024):
                f.write(chunk)

def unzip(game_path):
    print("extracting...")
    with ZipFile("temp.zip", "r") as ZO:
        ZO.extractall(game_path)
    os.remove("temp.zip")

def init():
    if not os.path.exists(os.path.join(os.getenv("LOCALAPPDATA"), "wot_modscout")):
        os.mkdir(os.path.join(os.getenv("LOCALAPPDATA"), "wot_modscout"))
    if not os.path.exists(os.path.join(os.getenv("LOCALAPPDATA"), "wot_modscout", "config")):
        ShowInfo("First Launch", "Hello and welcome to the ModScout!\nThis seems to be the first launch on your system.\nLets get going and set everything up")
        game_path = Select_folder("Select the WoT installation directory")
        with open(os.path.join(os.getenv("LOCALAPPDATA"), "wot_modscout", "config"), "w") as f:
            temp = {"game_path": game_path, "config_path": os.path.join(os.getenv("LOCALAPPDATA"), "wot_modscout", "config"), "mods": []}
            f.write(json.dumps(temp))

    with open(os.path.join(os.getenv("LOCALAPPDATA"), "wot_modscout", "config"), "r") as f:
        config = json.load(f)
    game_path = config["game_path"]
    print(f"Checking for WoT in {game_path}", end="")
    if not os.path.exists(game_path):
        print("WoT directory not existing")
        print("shutting down")
        sys.exit()

    print("  done")
    return config

def init_browser():
    path_to_config = os.path.join(os.getenv("LOCALAPPDATA"),"wot_modscout")
    if not os.path.exists(os.path.join(path_to_config, "chromedriver.exe")):
        print("No chromedriver detected...")
        print("Downloading chromedriver for Chrome version 81")
        with requests.get("https://chromedriver.storage.googleapis.com/81.0.4044.69/chromedriver_win32.zip", stream=True) as rq:
            rq.raise_for_status()
            with open(os.path.join(path_to_config, "chromedriver.zip"), "wb+") as f:
                for chunk in rq.iter_content(chunk_size=1024):
                    f.write(chunk)
        with ZipFile(os.path.join(path_to_config, "chromedriver.zip"), "r") as ZO:
            ZO.extractall(path_to_config)
        os.remove(os.path.join(path_to_config, "chromedriver.zip"))
    chromedriver_path = os.path.join(path_to_config, "chromedriver.exe")
    options = webdriver.ChromeOptions()
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    options.add_argument("--headless")
    driver = webdriver.Chrome(chromedriver_path, options=options)
    return driver

def get_version(game_path):
    elem = minidom.parse(os.path.join(game_path, "Version.xml"))
    version = elem.getElementsByTagName("version")[0].firstChild.nodeValue.strip()[2:10].strip()
    print(f"WoT version: {version}")
    return version

def get_mod_link(driver, mod):
    url = "https://wgmods.net/search/?title="+mod["title"].replace(" ", "%20")
    driver.get(url)
    try:
        web_mod = WebDriverWait(driver, 2).until(EC.presence_of_element_located((By.CLASS_NAME, "SearchResult_mod--39tkX")))
        web_mod.click()
    except:
        print("There is no current version available")
        return ""
    sleep(.5)
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, "ButtonYellowLarge_base--1U4NR")))
    markers = driver.find_elements_by_class_name("ModDetails_label--22VdG")
    for entry in markers:
       if "game version" in entry.text.lower():
           version = entry.text.lower()[14:]
           print(f"Version: {version}")
           mod["version"] = version
    buttons = driver.find_elements_by_class_name("ButtonYellowLarge_base--1U4NR")
    for entry in buttons:
        if "download mod" in entry.text.lower():
            button = entry
            break 
    button.click()
    if "mods are created by World of Tanks players".lower() in driver.page_source.lower():
        temp = driver.find_elements_by_class_name("ButtonYellowLarge_base--1U4NR")
        for entry in temp:
            if "got it" in entry.text.lower():
                entry.click()
                break
    link = driver.find_element_by_class_name("DialogDownload_hidden--RCW-c").get_attribute("href")
    return link

def update_mods():
    global config, group, modelements
    for entry in modelements:
        entry.update_icon("loading")
    group.widget.update()
    print(config["mods"])
    for entry in config["mods"]:
        mod_name = entry["title"]
        print(f"=={mod_name}==")
        temp = get_mod_link(driver, entry)
        if temp != "":
            download(temp)
            unzip(config["game_path"])
            for entry in modelements:
                if entry.mod_name == mod_name:
                    entry.update_icon("done")
                    group.widget.update()
                    break
            print("done")
        else:
            for entry in modelements:
                if entry.mod_name == mod_name:
                    entry.update_icon("cross")
                    group.widget.update()
                    break

def update_script(*args):
    mb = tkinter.messagebox.askquestion("Update Script?", "Are you sure you want to update the script?\nThis will overwrite the file!", icon="warning")
    if mb  == "no":
        return
    path = os.path.realpath(__file__)
    url = "https://gitlab.com/Eltonmaster/justfiles/-/raw/master/wot_modscout.py"
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        for chunk in r.iter_content(chunk_size=1024):
            with open(path, "wb") as f:
                f.write(chunk)


def on_closing():
    global driver, root
    print("closing down")
    driver.close()
    root.destroy()
    sys.exit()

def add_helper(*args):
    global group
    group.add_entry()




###Start###

root = tkinter.Tk()
root.title("WoT - ModScout")
root.protocol("WM_DELETE_WINDOW", on_closing)
group = Group(root, "Mods")
root.bind("<Return>", add_helper)
root.bind("u", update_script)
config = init()
modelements = group.populate(config)
update_button = UpdateButton(root)
driver = init_browser()

root.mainloop()
driver.close()
sys.exit()



#modlist = (config["mods"])
#version = get_version(config["game_path"])




