public class A_2 {
    public static class RatNumber {
        /* private variables */
        private long num=0l, denom=1l;

        /* constructors */
        public RatNumber() {}

        public RatNumber(long n, long d) {
            num = (d > 0) ? n : -n;
            denom = Math.abs(d);
            normalize();
        }

        public RatNumber(int n){
            num = n;
            denom = 1;
        }

        /* selectors */
        public long numerator()   { return num;   }
        public long denominator() { return denom; }

        /* public methods */
        public RatNumber add(RatNumber r) {
            long n,d;
            n = numerator() * r.denominator() + r.numerator() * denominator();
            d = denominator() * r.denominator();
            return new RatNumber(n,d);
        }

        public RatNumber minus(RatNumber r){
            long n, d;
            n = numerator() * r.denominator() - r.numerator() * denominator();
            d = denominator() * r.denominator();
            return new RatNumber(n, d);
        }

        public RatNumber mult(RatNumber r){
            long n, d;
            n = numerator() * r.numerator();
            d = denominator() * r.denominator();
            return new RatNumber(n, d);
        }

        public RatNumber div(RatNumber r){
            long n, d;
            n = numerator() * r.denominator();
            d = denominator() * r.numerator();
            return new RatNumber(n, d);
        }

        public void normalize() {
            long i = 2;
            long end = (num > denom ? denom : num);
            while (i <= end){
                if (num % i == 0 && denom % i == 0){
                    num = num / i;
                    denom = denom / i;
                    i = 2;
                    end = (num > denom ? denom : num);
                } else {
                    i++;
                }
            }
        }

        public String toString() {
            return num+"/"+denom;
        }

        public Boolean isZero() {
            return num == 0;
        }

        public Boolean equals(RatNumber a){
            RatNumber temp_a = new RatNumber(a.num, a.denom);
            RatNumber temp_b = new RatNumber(num, denom);

            temp_a.normalize();
            temp_b.normalize();

            return (temp_a.numerator() == temp_b.numerator() && temp_a.denominator() == temp_b.denominator());

        }
    }

    public static void main(String[] args) {

        RatNumber a = new RatNumber(1,2);
        System.out.println(a.toString());
        RatNumber b = new RatNumber(3,2);
        System.out.println(b.toString());

        RatNumber c = a.add(b);

        System.out.println(a + " + " + b + " = " + c);

        c = b.minus(a);
        System.out.println(b + " - " + a + " = " + c);

        c = a.mult(b);
        System.out.println(a + " * " + b + " = " + c);

        c = b.div(a);
        System.out.println(b + " / " + a + " = " + c);

        System.out.println(Boolean.toString(c.isZero()));

        c = new RatNumber(0,5);
        System.out.println(Boolean.toString(c.isZero()));
    }

}
